
import com.codingSchool.TSAManager.configuration.MvcWebApplicationInitializer;

import com.codingSchool.TSAManager.configuration.WebMvcConfiguration;
import com.codingSchool.TSAManager.services.*;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.text.ParseException;
import java.time.LocalDate;

@ContextConfiguration(classes = WebMvcConfiguration.class)
@ExtendWith(SpringExtension.class)
@WebAppConfiguration
public class TestSchedule {
    @Autowired
    ScheduleCreationService scheduleCreationService;

    @Autowired
    TeamCreationService teamCreationService;

    @Test
    public void testSchedule() throws ParseException {

        LocalDate date = LocalDate.of(2013, 12, 01);
        String x = scheduleCreationService.createSchedule(date);
        assertEquals("dsadas", x);
    }

    @Test
    public void createTeams(){
        teamCreationService.creatingTeam();

    }
}
