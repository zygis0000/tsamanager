package com.codingSchool.TSAManager.services;

import com.codingSchool.TSAManager.dao.ValidationDao;
import com.codingSchool.TSAManager.entities.Validation;
import com.codingSchool.TSAManager.services.interfaces.IValidationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service
public class ValidationService implements IValidationService {

    @Autowired
    private ValidationDao validationDao;

    @Override
    public String addValidation(Validation validation) {
        return validationDao.addValidation(validation);
    }

    @Override
    public LocalDate getValidationDate(String uuid, String certificateId) {
        return validationDao.getValidationDate(uuid, certificateId);
    }
}
