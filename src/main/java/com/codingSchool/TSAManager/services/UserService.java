package com.codingSchool.TSAManager.services;

import com.codingSchool.TSAManager.dao.UserDao;
import com.codingSchool.TSAManager.entities.User;
import com.codingSchool.TSAManager.services.interfaces.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService implements IUserService {

    @Autowired
    private UserDao userDao;

    @Override
    public boolean addUser(User user, String role) {
        return userDao.addUser(user, role);
    }

    @Override
    public boolean deleteUser(String userId) {
        return userDao.deleteUser(userId);
    }

    @Override
    public List<User> getAllUsers() {
        return userDao.getAllUsers();
    }
}

