package com.codingSchool.TSAManager.services;

import com.codingSchool.TSAManager.dao.CertificateDao;
import com.codingSchool.TSAManager.entities.Certificate;
import com.codingSchool.TSAManager.entities.Validation;
import com.codingSchool.TSAManager.services.interfaces.ICertificateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CertificateService implements ICertificateService {

    @Autowired
    private CertificateDao certificateDao;

    @Override
    public Certificate getEmployeeCertificates(String uuid) {
        return certificateDao.getEmployeeCertificates(uuid);
    }

    @Override
    public Certificate getEmployeeCertificate(String id, String certificateId) {
        return certificateDao.getEmployeeCertificate(id, certificateId);
    }

    @Override
    public int addCertificateToEmployee(String uuid, String certificateId, Validation validation) {
        return certificateDao.addCertificateToEmployee(uuid, certificateId, validation);
    }

    @Override
    public int deleteCertificateFromEmployee(String uuid, String certificateId) {
        return certificateDao.deleteCertificateFromEmployee(uuid, certificateId);
    }

    @Override
    public int countEmployeeCertifcates(String employeeId) {
        return certificateDao.countEmployeeCertifcates(employeeId);
    }
}
