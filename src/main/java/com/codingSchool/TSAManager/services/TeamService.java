package com.codingSchool.TSAManager.services;

import com.codingSchool.TSAManager.dao.TeamDao;
import com.codingSchool.TSAManager.entities.Team;
import com.codingSchool.TSAManager.services.interfaces.ITeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TeamService implements ITeamService {

    @Autowired
    private TeamDao teamDao;

    @Override
    public long createTeam() {
        return teamDao.createTeam();
    }

    @Override
    public long deleteTeam(String teamId) {
        return teamDao.deleteTeam(teamId);
    }

    @Override
    public long addEmployeeToTeam(long teamID, String employeeId) {
        return teamDao.addEmployeeToTeam(teamID, employeeId);
    }

    @Override
    public List<Long> getAllTeamsIds() {
        return teamDao.getAllTeamsIds();
    }

    @Override
    public int deleteEmployeeFromTeam(String employeeId) {
        return teamDao.deleteEmployeeFromTeam(employeeId);
    }

    @Override
    public List<Team> getAllTeams() {
        return teamDao.getAllTeams();
    }

    @Override
    public Team getTeam(long teamId) {
        return teamDao.getTeam(teamId);
    }

    @Override
    public int countEmployeesInTeam(String teamId) {
        return teamDao.countEmployeesInTeam(teamId);
    }

    @Override
    public int countTeams() {
        return teamDao.countTeams();
    }


}
