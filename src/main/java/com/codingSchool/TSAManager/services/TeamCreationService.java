package com.codingSchool.TSAManager.services;

import com.codingSchool.TSAManager.utilities.Gender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TeamCreationService {

    @Autowired
    private EmployeeService employeeService;

    @Autowired
    private TeamService teamService;

    @Autowired
    private CertificateService certificateService;


    private boolean checkRightAmountOfEmployee() {
        int requiredAmount = 10;
        int amountOfEmployee = employeeService.countEmployeeByGender(Gender.MALE.toString())
                + employeeService.countEmployeeByGender(Gender.FEMALE.toString());
        if (amountOfEmployee >= requiredAmount) {
            return true;
        }
        return false;
    }

    private List<String> checkEmployeeSertificate(List<String> employeeList) {
        int verify;
        String id;
        for (int i = 0; i < employeeList.size(); i++) {
            id = employeeList.get(i);
            verify = certificateService.countEmployeeCertifcates(id);
            if (verify != 9) {
                employeeList.remove(i);
            }
        }
        return employeeList;
    }


    public void creatingTeam() {
        int amount = teamService.countTeams();
        long teamId;
        int teamsToCreate = 4 - amount;
        if (amount < 4) {
            for (int i = 0; i < teamsToCreate; i++) {
                if (checkRightAmountOfEmployee()) {
                    List<String> male = employeeService.getIdsOfEmployeesByGenderWithoutTeam(Gender.MALE.toString().toLowerCase());
                    List<String> female = employeeService.getIdsOfEmployeesByGenderWithoutTeam(Gender.FEMALE.toString().toLowerCase());
                    List<String> verifiedM = checkEmployeeSertificate(male);
                    List<String> verifiedF = checkEmployeeSertificate(female);
                    if (verifiedF.size() >= 5 && verifiedM.size() >= 5) {
                        teamId = teamService.createTeam();
                        for (int j = 0; j < 5; j++) {
                            teamService.addEmployeeToTeam(teamId, verifiedF.get(j));
                            teamService.addEmployeeToTeam(teamId, verifiedM.get(j));
                        }

                    }

                }

            }

        }
    }
}
