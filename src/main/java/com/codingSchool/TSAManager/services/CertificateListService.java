package com.codingSchool.TSAManager.services;

import com.codingSchool.TSAManager.dao.CertificateListDAO;
import com.codingSchool.TSAManager.entities.CertificateList;
import com.codingSchool.TSAManager.services.interfaces.ICertificateListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CertificateListService implements ICertificateListService {

    @Autowired
    private CertificateListDAO certificateListDao;

    @Override
    public String addCertification(CertificateList certificate) {
        return certificateListDao.addCertification(certificate);
    }

    @Override
    public CertificateList getCertificate(String uuid) {
        return certificateListDao.getCertificate(uuid);
    }

    @Override
    public List<CertificateList> getAllCertificates() {
        return certificateListDao.getAllCertificates();
    }

    @Override
    public int deleteCertificate(String uuid) {
        return certificateListDao.deleteCertificate(uuid);
    }

    @Override
    public int updateSertificate(String uuid, String title) {
        return certificateListDao.updateSertificate(uuid, title);
    }
}
