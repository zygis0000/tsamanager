package com.codingSchool.TSAManager.services.interfaces;

import com.codingSchool.TSAManager.entities.User;

import java.util.List;

public interface IUserService {
    boolean addUser(User user, String role);
    boolean deleteUser(String userId);
    List<User> getAllUsers();
}
