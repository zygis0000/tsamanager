package com.codingSchool.TSAManager.services.interfaces;

import com.codingSchool.TSAManager.entities.Day;
import com.codingSchool.TSAManager.entities.Schedule;

import java.time.LocalDate;
import java.util.List;

public interface IScheduleService {
    long createScheduleForTeamAtDay(Schedule schedule);
    long deleteScheduleForTeamAtDay(long teamId, long dayId);
    Day getTeamScheduleForDay(long teamid, LocalDate date);
    List<Day> getmonthScheduleForTeam(LocalDate date, long teamId);
    List<Day> getmonthSchedule(LocalDate date);
}
