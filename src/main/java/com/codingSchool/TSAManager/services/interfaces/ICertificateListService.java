package com.codingSchool.TSAManager.services.interfaces;

import com.codingSchool.TSAManager.entities.CertificateList;

import java.util.List;


public interface ICertificateListService {
    String addCertification(CertificateList certificate);
    CertificateList getCertificate(String uuid);
    List<CertificateList> getAllCertificates();
    int deleteCertificate(String uuid);
    int updateSertificate(String uuid, String title);

}
