package com.codingSchool.TSAManager.services.interfaces;

import com.codingSchool.TSAManager.entities.Day;
import java.time.LocalDate;
import java.util.List;

public interface IDayService {
    long createDay(Day day);
    LocalDate getDate(long id);
    Day getDay(long id);
    String checkShift(long id);
    String checkFlag(long id);
    int deleteDay(long id);
    List<Day> getByYearAndMonthShift(LocalDate date, long teamid);
    String checkbyDateAndTeamIdShift(LocalDate date, long teamId);
    String checkbyDateAndTeamIdFlag(LocalDate date, long teamId);
}
