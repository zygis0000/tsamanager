package com.codingSchool.TSAManager.services.interfaces;

import com.codingSchool.TSAManager.entities.Schedule;

import java.time.LocalDate;


public interface IScheduleCreationService {

    boolean checkIfChosenMonthHaveSchedule();

    boolean checkIfThereIsRightAmountOfTeamsToCreateSchedule();

    boolean checkLastDayOfMonthOfTeam(long teamId);

    Schedule createScheduleOfMonth(LocalDate date);


}
