package com.codingSchool.TSAManager.services.interfaces;

import com.codingSchool.TSAManager.entities.Contacts;
import com.codingSchool.TSAManager.entities.Employee;

import java.util.List;


public interface IEmployeeService {
    String getPosition(String id);
    int changePosition(String uuid, String position);
    String addEmployee(Employee employee, Contacts contacts);
    int deleteEmployee(String uuid);
    int addEmployeeContacts(Contacts contacts, String uuid);
    List<Employee> getAllByGender(String gender);
    String getEmployeeGender(String uuid);
    Employee getEmployeeById(String uuid);
    List<String> getAllIdsOfEmployees();
    List<Employee> getAllEmployees();
    int countEmployeeByGender(String gender);
    List<String> getIdsOfEmployeesByGenderWithoutTeam(String gender);

}
