package com.codingSchool.TSAManager.services.interfaces;

import com.codingSchool.TSAManager.entities.Team;

import java.util.List;


public interface ITeamService {
    long createTeam();
    long deleteTeam(String teamId);
    long addEmployeeToTeam(long teamID, String employeeId);
    int deleteEmployeeFromTeam(String employeeId);
    List<Team> getAllTeams();
    Team getTeam(long teamId);
    int countEmployeesInTeam(String teamId);
    int countTeams();
    List<Long> getAllTeamsIds();
}
