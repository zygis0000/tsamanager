package com.codingSchool.TSAManager.services.interfaces;

import com.codingSchool.TSAManager.entities.Validation;

import java.time.LocalDate;

public interface IValidationService {
    String addValidation(Validation validation);
    LocalDate getValidationDate(String uuid, String sertificateId);
}
