package com.codingSchool.TSAManager.services.interfaces;

import com.codingSchool.TSAManager.entities.Certificate;
import com.codingSchool.TSAManager.entities.Validation;


public interface ICertificateService {

    Certificate getEmployeeCertificates(String uuid);
    Certificate getEmployeeCertificate(String uuid, String certificateId);
    int addCertificateToEmployee(String uuid, String certificateId, Validation validation);
    int deleteCertificateFromEmployee(String uuid, String certificateId);
    int countEmployeeCertifcates(String employeeId);

}
