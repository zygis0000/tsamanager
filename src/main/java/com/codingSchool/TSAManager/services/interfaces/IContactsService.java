package com.codingSchool.TSAManager.services.interfaces;

import com.codingSchool.TSAManager.entities.Contacts;

import java.util.List;


public interface IContactsService {
    List<Contacts> getEmployeeContacts(String uuid);
}
