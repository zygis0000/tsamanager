package com.codingSchool.TSAManager.services;

import com.codingSchool.TSAManager.dao.ScheduleDao;
import com.codingSchool.TSAManager.entities.Day;
import com.codingSchool.TSAManager.entities.Schedule;
import com.codingSchool.TSAManager.services.interfaces.IScheduleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
public class ScheduleService implements IScheduleService {

    @Autowired
    private ScheduleDao scheduleDao;

    @Override
    public long createScheduleForTeamAtDay(Schedule schedule) {
        return scheduleDao.createScheduleForTeamAtDay(schedule);
    }

    @Override
    public long deleteScheduleForTeamAtDay(long teamId, long dayId) {
        return scheduleDao.deleteScheduleForTeamAtDay(teamId, dayId);
    }

    @Override
    public Day getTeamScheduleForDay(long teamid, LocalDate date) {
        return scheduleDao.getTeamScheduleForDay(teamid, date);
    }

    @Override
    public List<Day> getmonthScheduleForTeam(LocalDate date, long teamId) {
        return scheduleDao.getmonthScheduleForTeam(date, teamId);
    }

    @Override
    public List<Day> getmonthSchedule(LocalDate date) {
        return scheduleDao.getmonthSchedule(date);
    }
}
