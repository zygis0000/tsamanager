package com.codingSchool.TSAManager.services;

import com.codingSchool.TSAManager.dao.EmployeeDao;
import com.codingSchool.TSAManager.entities.Contacts;
import com.codingSchool.TSAManager.entities.Employee;
import com.codingSchool.TSAManager.services.interfaces.IEmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeService implements IEmployeeService {

    @Autowired
    private EmployeeDao employeeDao;

    @Override
    public String getPosition(String uuid) {
        return employeeDao.getPosition(uuid);
    }

    @Override
    public int changePosition(String uuid, String position) {
        return employeeDao.changePosition(uuid, position);
    }

    @Override
    public String addEmployee(Employee employee, Contacts contacts) {
        return employeeDao.addEmployee(employee, contacts);
    }

    @Override
    public int deleteEmployee(String uuid) {
        return employeeDao.deleteEmployee(uuid);
    }

    @Override
    public int addEmployeeContacts(Contacts contacts, String uuid) {
        return employeeDao.addEmployeeContacts(contacts, uuid);
    }

    @Override
    public List<Employee> getAllByGender(String gender) {

        return employeeDao.getAllByGender(gender);
    }

    @Override
    public String getEmployeeGender(String uuid) {
        return employeeDao.getEmployeeGender(uuid);
    }

    @Override
    public Employee getEmployeeById(String uuid) {
        return employeeDao.getEmployeeById(uuid);
    }

    @Override
    public List<String> getAllIdsOfEmployees() {
        return employeeDao.getAllIdsOfEmployees();
    }

    @Override
    public List<Employee> getAllEmployees() {
        return employeeDao.getAllEmployees();
    }

    @Override
    public int countEmployeeByGender(String gender) {
        return employeeDao.countEmployeeByGender(gender);
    }

    @Override
    public List<String> getIdsOfEmployeesByGenderWithoutTeam(String gender) {
        return employeeDao.getIdsOfEmployeesByGenderWithoutTeam(gender);
    }


}
