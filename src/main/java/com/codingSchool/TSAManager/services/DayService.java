package com.codingSchool.TSAManager.services;

import com.codingSchool.TSAManager.dao.DayDao;
import com.codingSchool.TSAManager.entities.Day;
import com.codingSchool.TSAManager.services.interfaces.IDayService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
public class DayService implements IDayService {

    @Autowired
    private DayDao dayDao;

    @Override
    public long createDay(Day day) {
        return dayDao.createDay(day);
    }

    @Override
    public LocalDate getDate(long id) {
        return dayDao.getDate(id);
    }

    @Override
    public Day getDay(long id) {
        return dayDao.getDay(id);
    }

    @Override
    public String checkShift(long id) {
        return dayDao.checkShift(id);
    }

    @Override
    public String checkFlag(long id) {
        return dayDao.checkFlag(id);
    }

    @Override
    public int deleteDay(long id) {
        return dayDao.deleteDay(id);
    }

    @Override
    public List<Day> getByYearAndMonthShift(LocalDate date, long teamid) {
        return dayDao.getByYearAndMonthShift(date, teamid);
    }

    @Override
    public String checkbyDateAndTeamIdShift(LocalDate date, long teamId) {
        return dayDao.checkbyDateAndTeamIdShift(date, teamId);
    }

    @Override
    public String checkbyDateAndTeamIdFlag(LocalDate date, long teamId) {
        return dayDao.checkbyDateAndTeamIdFlag(date, teamId);
    }
}
