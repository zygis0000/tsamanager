package com.codingSchool.TSAManager.services;

import com.codingSchool.TSAManager.entities.Day;
import com.codingSchool.TSAManager.entities.Schedule;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.temporal.ChronoField;
import java.util.List;

@Service
public class ScheduleCreationService {

    @Autowired
    private ScheduleService scheduleService;

    @Autowired
    private TeamService teamService;

    @Autowired
    private DayService dayService;

    private Day day;

    private Schedule schedule;

    private boolean validationOfMonth(LocalDate date) {
        List<Day> list = scheduleService.getmonthSchedule(date);
        if (list.isEmpty()) {
            return true;
        }
        return false;
    }

    private void teamDaySetterInSchedule(long teamId, int rowNumberOfTeamId, int columnNumberOfScheduleArray, LocalDate date) {
        String sch[][] = {
                {"d1", "d2", "n1", "n2", "h1", "h2", "h3", "h4"},
                {"n1", "n2", "d1", "d2", "h1", "h2", "h3", "h4"},
                {"h1", "h2", "h3", "h4", "d1", "d2", "n1", "n2"},
                {"h1", "h2", "h3", "h4", "n1", "n2", "d1", "d2"}
        };
        day = new Day();
        schedule = new Schedule();
        day.setShift(String.valueOf(sch[rowNumberOfTeamId][columnNumberOfScheduleArray].charAt(0)));
        day.setFlag(String.valueOf(sch[rowNumberOfTeamId][columnNumberOfScheduleArray].charAt(1)));
        day.setDate(date);
        long key = dayService.createDay(day);
        schedule.setTeamId(teamId);
        schedule.setDayId(key);
        scheduleService.createScheduleForTeamAtDay(schedule);
    }

    private void scheduleCreationEngine(LocalDate date, int monthSize, int step) {

        List<Long> teams = teamService.getAllTeamsIds();
        int teamSize = teams.size();
        for (int j = 0; j < teamSize; j++) {
            int k = step;
            for (int i = 0; i < monthSize; i++) {
                if (k == 8) {
                    k = 0;
                }
                teamDaySetterInSchedule(teams.get(j), j, k, date.plusDays(i));
                k++;
            }

        }
    }

    private void scheduleCreation(LocalDate date, int step) {
        date = date.plusDays(1);
        int monthDaysToCreate = date.lengthOfMonth() - date.get(ChronoField.DAY_OF_MONTH) + 1;
        if (monthDaysToCreate == 0) {
            scheduleCreationEngine(date, 1, step);
        } else {
            scheduleCreationEngine(date, monthDaysToCreate, step);
        }
    }

    private boolean dateCheckForTeam(LocalDate date, long teamId) {
        Day day = scheduleService.getTeamScheduleForDay(teamId, date);
        if (day == null) {
            return true;
        }
        return false;
    }


    public String createSchedule(LocalDate date) {
        List<Long> teams = teamService.getAllTeamsIds();
        if (!teams.isEmpty()) {
            long firstTeamId = teams.get(0);
            if (dateCheckForTeam(date, firstTeamId)) {
                if (validationOfMonth(date)) {
                    if (teams.size() == 4) {
                        date = date.minusDays(1);
                        String flag = dayService.checkbyDateAndTeamIdFlag(date, firstTeamId);
                        String shift = dayService.checkbyDateAndTeamIdShift(date, firstTeamId);
                        if (flag == null && shift == null) {
                            scheduleCreation(date, 0);
                        } else if (flag.equals("1") && shift.equals("d")) {
                            scheduleCreation(date, 1);
                        } else if (flag.equals("2") && shift.equals("d")) {
                            scheduleCreation(date, 2);
                        } else if (flag.equals("1") && shift.equals("n")) {
                            scheduleCreation(date, 3);
                        } else if (flag.equals("2") && shift.equals("n")) {
                            scheduleCreation(date, 4);
                        } else if (flag.equals("1") && shift.equals("h")) {
                            scheduleCreation(date, 5);
                        } else if (flag.equals("2") && shift.equals("h")) {
                            scheduleCreation(date, 6);
                        } else if (flag.equals("3") && shift.equals("h")) {
                            scheduleCreation(date, 7);
                        } else if (flag.equals("4") && shift.equals("h")) {
                            scheduleCreation(date, 8);
                        }
                        return "Build passed";
                    }
                    return "Not enough teams";
                }
                return "Some part of Month has schedule";
            }
            return "Can't create schedule on this day";
        }
        return "Teams not exist";
    }


}
