package com.codingSchool.TSAManager.services;

import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Properties;


@Service
public class PropertyService {

//        private final static Logger LOGGER = LoggerFactory.getLogger(PropertyManager.class);

    public Properties getDbConfigFile() {
        Properties databaseProps = new Properties();
        try {
//                LOGGER.info("Reading ***** dbconfiguration.properties ***** ");
            databaseProps.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("database.properties"));
        } catch (IOException e) {
//                LOGGER.info("Failed to read ***** dbconfiguration.properties ***** ");
            e.printStackTrace();
        }
        return databaseProps;
    }

}