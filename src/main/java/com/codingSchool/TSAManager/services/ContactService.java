package com.codingSchool.TSAManager.services;

import com.codingSchool.TSAManager.dao.ContactsDao;
import com.codingSchool.TSAManager.entities.Contacts;
import com.codingSchool.TSAManager.services.interfaces.IContactsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ContactService implements IContactsService {

    @Autowired
    private ContactsDao contactsDao;

    @Override
    public List<Contacts> getEmployeeContacts(String uuid) {
        return contactsDao.getEmployeeContacts(uuid);
    }
}
