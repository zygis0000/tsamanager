package com.codingSchool.TSAManager.workflow;

import com.codingSchool.TSAManager.entities.Certificate;
import com.codingSchool.TSAManager.entities.Contacts;
import com.codingSchool.TSAManager.entities.Employee;
import com.codingSchool.TSAManager.entities.Validation;
import com.codingSchool.TSAManager.services.CertificateService;
import com.codingSchool.TSAManager.services.ContactService;
import com.codingSchool.TSAManager.services.EmployeeService;
import com.codingSchool.TSAManager.workflow.interfaces.EmployeeWorkflowInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeWorkflow implements EmployeeWorkflowInterface {

    @Autowired
    private CertificateService certificateService;

    @Autowired
    private EmployeeService employeeService;

    @Autowired
    private ContactService contactService;

    @Override
    public List<Employee> getAllEmployees() {
        return employeeService.getAllEmployees();
    }

    @Override
    public String getPosition(String id) {
        return employeeService.getPosition(id);
    }

    @Override
    public int changePosition(String uuid, String position) {
        return employeeService.changePosition(uuid,position);
    }

    @Override
    public String addEmployee(Employee employee, Contacts contacts) {
        return employeeService.addEmployee(employee, contacts);
    }

    @Override
    public int deleteEmployee(String uuid) {
        return employeeService.deleteEmployee(uuid);
    }

    @Override
    public int addEmployeeContacts(Contacts contacts, String uuid) {
        return employeeService.addEmployeeContacts(contacts, uuid);
    }

    @Override
    public List<Employee> getAllByGender(String gender) {
        return employeeService.getAllByGender(gender);
    }

    @Override
    public String getEmployeeGender(String uuid) {
        return employeeService.getEmployeeGender(uuid);
    }

    @Override
    public Employee getEmployeeById(String uuid) {
        return employeeService.getEmployeeById(uuid);
    }

    @Override
    public List<Contacts> getEmployeeContacts(String uuid) {
        return contactService.getEmployeeContacts(uuid);
    }

    @Override
    public Certificate getEmployeeCertificates(String uuid) {
        return certificateService.getEmployeeCertificates(uuid);
    }

    @Override
    public Certificate getEmployeeCertificate(String uuid, String certificateId) {
        return certificateService.getEmployeeCertificate(uuid, certificateId);
    }

    @Override
    public int addCertificateToEmployee(String uuid, String certificateId, Validation validation) {
        return certificateService.addCertificateToEmployee(uuid, certificateId, validation );
    }

    @Override
    public int deleteCertificateFromEmployee(String uuid, String certificateId) {
        return certificateService.deleteCertificateFromEmployee(uuid,certificateId);
    }
}
