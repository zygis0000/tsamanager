package com.codingSchool.TSAManager.workflow;

import com.codingSchool.TSAManager.entities.Team;
import com.codingSchool.TSAManager.services.TeamCreationService;
import com.codingSchool.TSAManager.services.TeamService;
import com.codingSchool.TSAManager.workflow.interfaces.TeamsWorkflowInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TeamWorkflow implements TeamsWorkflowInterface {

    @Autowired
    private TeamCreationService creationService;

    @Autowired
    private TeamService teamService;

    @Override
    public void createTeam() {
       creationService.creatingTeam();
    }

    @Override
    public long deleteTeam(String teamId) {
        return teamService.deleteTeam(teamId);
    }

    @Override
    public long addEmployeeToTeam(long teamID, String employeeId) {
        return teamService.addEmployeeToTeam(teamID, employeeId);
    }

    @Override
    public int deleteEmployeeFromTeam(String employeeId) {
        return teamService.deleteEmployeeFromTeam(employeeId);
    }

    @Override
    public List<Team> getAllTeams() {
        return teamService.getAllTeams();
    }

    @Override
    public Team getTeam(long teamId) {
        return teamService.getTeam(teamId);
    }
}
