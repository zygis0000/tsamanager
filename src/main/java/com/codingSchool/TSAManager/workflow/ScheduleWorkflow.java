package com.codingSchool.TSAManager.workflow;

import com.codingSchool.TSAManager.entities.Day;
import com.codingSchool.TSAManager.services.ScheduleCreationService;
import com.codingSchool.TSAManager.services.ScheduleService;
import com.codingSchool.TSAManager.workflow.interfaces.ScheduleWorkflowInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
public class ScheduleWorkflow implements ScheduleWorkflowInterface {

    @Autowired
    private ScheduleCreationService creationService;

    @Autowired
    private ScheduleService scheduleService;

    @Override
    public String createSchedule(LocalDate date)  {
        return creationService.createSchedule(date);
    }

    @Override
    public long deleteScheduleForTeamAtDay(long teamId, long dayId) {
        return scheduleService.deleteScheduleForTeamAtDay(teamId, dayId);
    }

    @Override
    public Day getTeamScheduleForDay(long teamid, LocalDate date) {
        return scheduleService.getTeamScheduleForDay(teamid, date);
    }

    @Override
    public List<Day> getmonthScheduleForTeam(LocalDate date, long teamId) {
        return scheduleService.getmonthScheduleForTeam(date, teamId);
    }

    @Override
    public List<Day> getmonthSchedule(LocalDate date) {
        return scheduleService.getmonthSchedule(date);
    }
}
