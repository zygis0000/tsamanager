package com.codingSchool.TSAManager.workflow.interfaces;

import com.codingSchool.TSAManager.entities.User;

import java.util.List;

public interface UserWorkflowInterface {
    boolean addUser(User user, String role);
    boolean deleteUser(String userId);
    List<User> getAllUsers();
}
