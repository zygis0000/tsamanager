package com.codingSchool.TSAManager.workflow.interfaces;

import com.codingSchool.TSAManager.entities.Team;

import java.util.List;

public interface TeamsWorkflowInterface {
    void createTeam();
    long deleteTeam(String teamId);
    long addEmployeeToTeam(long teamID, String employeeId);
    int deleteEmployeeFromTeam(String employeeId);
    List<Team> getAllTeams();
    Team getTeam(long teamId);
}
