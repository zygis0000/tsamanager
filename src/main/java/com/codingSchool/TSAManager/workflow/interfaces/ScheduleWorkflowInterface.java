package com.codingSchool.TSAManager.workflow.interfaces;

import com.codingSchool.TSAManager.entities.Day;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
public interface ScheduleWorkflowInterface {
    String createSchedule(LocalDate date);
    long deleteScheduleForTeamAtDay(long teamId, long dayId);
    Day getTeamScheduleForDay(long teamid, LocalDate date);
    List<Day> getmonthScheduleForTeam(LocalDate date, long teamId);
    List<Day> getmonthSchedule(LocalDate date);

}
