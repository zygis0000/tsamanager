package com.codingSchool.TSAManager.workflow.interfaces;

import com.codingSchool.TSAManager.entities.Certificate;
import com.codingSchool.TSAManager.entities.Contacts;
import com.codingSchool.TSAManager.entities.Employee;
import com.codingSchool.TSAManager.entities.Validation;

import java.util.List;

public interface EmployeeWorkflowInterface {
    List<Employee> getAllEmployees();
    String getPosition(String id);
    int changePosition(String uuid, String position);
    String addEmployee(Employee employee, Contacts contacts);
    int deleteEmployee(String uuid);
    int addEmployeeContacts(Contacts contacts, String uuid);
    List<Employee> getAllByGender(String gender);
    String getEmployeeGender(String uuid);
    Employee getEmployeeById(String uuid);
    List<Contacts> getEmployeeContacts(String uuid);
    Certificate getEmployeeCertificates(String uuid);
    Certificate getEmployeeCertificate(String uuid, String sertificateId);
    int addCertificateToEmployee(String uuid, String sertificateId, Validation validation);
    int deleteCertificateFromEmployee(String uuid, String sertificateId);

}
