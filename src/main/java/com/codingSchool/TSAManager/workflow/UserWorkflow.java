package com.codingSchool.TSAManager.workflow;

import com.codingSchool.TSAManager.entities.User;
import com.codingSchool.TSAManager.services.UserService;
import com.codingSchool.TSAManager.workflow.interfaces.UserWorkflowInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserWorkflow implements UserWorkflowInterface {

    @Autowired
    private UserService userService;

    @Override
    public boolean addUser(User user, String role) {
        return userService.addUser(user, role);
    }

    @Override
    public boolean deleteUser(String userId) {
        return userService.deleteUser(userId);
    }

    @Override
    public List<User> getAllUsers() {
        return userService.getAllUsers();
    }
}
