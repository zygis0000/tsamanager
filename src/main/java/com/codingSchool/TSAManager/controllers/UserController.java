package com.codingSchool.TSAManager.controllers;

import com.codingSchool.TSAManager.entities.User;
import com.codingSchool.TSAManager.workflow.UserWorkflow;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserWorkflow userWorkflow;

    @RequestMapping("/addUser/{role}")
    public boolean addUser(@ModelAttribute User user, @PathVariable String role) {
        return userWorkflow.addUser(user, role);
    }

    @RequestMapping("/deleteUser/{userId}")
    public boolean deleteUser(@PathVariable String userId) {
        return userWorkflow.deleteUser(userId);
    }

    @RequestMapping("/getAllUsers")
    public List<User> getAllUsers() {
        return userWorkflow.getAllUsers();
    }
}
