package com.codingSchool.TSAManager.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@RestController
public class PageController {

    @RequestMapping("/login")
    public ModelAndView getLoginPage(ModelAndView model) {
        model.setViewName("login");
        return model;
    }

    @RequestMapping("/admin")
    public ModelAndView getAdminPage(ModelAndView model) {
        model.setViewName("admin");
        return model;
    }

    @RequestMapping("/index")
    public ModelAndView getUserPage(ModelAndView model) {
        model.setViewName("index");
        return model;
    }
}
