package com.codingSchool.TSAManager.controllers;


import com.codingSchool.TSAManager.entities.Certificate;
import com.codingSchool.TSAManager.entities.Contacts;
import com.codingSchool.TSAManager.entities.Employee;
import com.codingSchool.TSAManager.entities.Validation;
import com.codingSchool.TSAManager.workflow.EmployeeWorkflow;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;


@RestController
@RequestMapping("/employee")
public class EmployeeController {

    @Autowired
    private EmployeeWorkflow employeeWorkflow;



    public EmployeeController(EmployeeWorkflow employeeWorkflow) {
        this.employeeWorkflow = employeeWorkflow;
    }


    @RequestMapping(value = "/addEmployee", method = RequestMethod.POST)
    public String addEmployee(@Valid @ModelAttribute Employee employee, @Valid @ModelAttribute Contacts contacts, BindingResult result) {
        if (result.hasErrors()) {
            return "error";
        }
        employeeWorkflow.addEmployee(employee, contacts);
        return "admin";
    }

    @RequestMapping("/getPosition/{id}")
    public String getPosition(@PathVariable String id) {
        return employeeWorkflow.getPosition(id);
    }

    @RequestMapping("/changePosition/{uuid}/{position}")
    public int changePosition(@PathVariable String uuid, @PathVariable String position) {
        return employeeWorkflow.changePosition(uuid, position);
    }

    @RequestMapping("/deleteEmployee/{uuid}")
    public int deleteEmployee(@PathVariable String uuid) {
        return employeeWorkflow.deleteEmployee(uuid);
    }

    @RequestMapping("/addEmployeeContacts/{uuid}")
    public int addEmployeeContacts(@Valid @ModelAttribute Contacts contacts, @PathVariable String uuid) {
        return employeeWorkflow.addEmployeeContacts(contacts, uuid);
    }

    @RequestMapping("/getAllByGender/{gender}")
    public List<Employee> getAllByGender(@PathVariable String gender) {
        return employeeWorkflow.getAllByGender(gender);
    }

    @RequestMapping("/getAllEmployees")
    public List<Employee> getAllEmployees() {
        return employeeWorkflow.getAllEmployees();
    }

    @RequestMapping("/getEmployeeById/{uuid}")
    public Employee getEmployeeById(@PathVariable String uuid) {
        return employeeWorkflow.getEmployeeById(uuid);
    }

    @RequestMapping("/getEmployeeContacts/{uuid}")
    public List<Contacts> getEmployeeContacts(@PathVariable String uuid) {
        return employeeWorkflow.getEmployeeContacts(uuid);
    }

    @RequestMapping("/getEmployeeCertificates/{uuid}")
    public Certificate getEmployeeCertificates(@PathVariable String uuid) {
        return employeeWorkflow.getEmployeeCertificates(uuid);
    }

    @RequestMapping("/getEmployeeCertificate/{uuid}/{certificateId}")
    public Certificate getEmployeeCertificate(@PathVariable String uuid, @PathVariable String certificateId) {
        return employeeWorkflow.getEmployeeCertificate(uuid, certificateId);
    }

    @RequestMapping("/addCertificateToEmployee/{uuid}/{certificateId}")
    public int addCertificateToEmployee(@PathVariable String uuid, @PathVariable String certificateId, @Valid @ModelAttribute Validation validation) {
        return employeeWorkflow.addCertificateToEmployee(uuid, certificateId, validation);
    }

    @RequestMapping("/deleteCertificateFromEmployee/{uuid}/{certificateId}")
    public int deleteCertificateFromEmployee(@PathVariable String uuid, @PathVariable String certificateId) {
        return employeeWorkflow.deleteCertificateFromEmployee(uuid, certificateId);
    }
}
