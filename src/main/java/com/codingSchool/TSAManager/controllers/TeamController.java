package com.codingSchool.TSAManager.controllers;

import com.codingSchool.TSAManager.entities.Team;
import com.codingSchool.TSAManager.workflow.TeamWorkflow;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/team")
public class TeamController {

    @Autowired
    private TeamWorkflow teamWorkflow;

    @RequestMapping("/createTeam")
    public void createTeam() {
       teamWorkflow.createTeam();
    }

    @RequestMapping("/deleteTeam/{teamId}")
    public long deleteTeam(@PathVariable String teamId) {
        return teamWorkflow.deleteTeam(teamId);
    }

    @RequestMapping("/addEmployeeToTeam/{teamID}/{employeeId}")
    public long addEmployeeToTeam(@PathVariable long teamID, @PathVariable String employeeId) {
        return teamWorkflow.addEmployeeToTeam(teamID, employeeId);
    }

    @RequestMapping("deleteEmployeeFromTeam/{employeeId}")
    public int deleteEmployeeFromTeam(@PathVariable String employeeId) {
        return teamWorkflow.deleteEmployeeFromTeam(employeeId);
    }

    @RequestMapping("/getAllTeams")
    public List<Team> getAllTeams() {
        return teamWorkflow.getAllTeams();
    }

    @RequestMapping("/getTeam/{teamId}")
    public Team getTeam(@PathVariable long teamId) {

        return teamWorkflow.getTeam(teamId);
    }
}
