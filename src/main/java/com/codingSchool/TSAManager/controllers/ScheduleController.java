package com.codingSchool.TSAManager.controllers;

import com.codingSchool.TSAManager.entities.Day;
import com.codingSchool.TSAManager.workflow.ScheduleWorkflow;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.util.List;

@RestController
@RequestMapping("/schedule")
public class ScheduleController {

    @Autowired
    private ScheduleWorkflow scheduleWorkflow;

    @RequestMapping("/createSchedule/{date}")
    public String createSchedule(@PathVariable @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate date)  {
        return scheduleWorkflow.createSchedule(date);
    }

    @RequestMapping("/deleteScheduleForTeamAtDay/{teamId}/{dayId}")
    public long deleteScheduleForTeamAtDay(@PathVariable long teamId, @PathVariable long dayId) {
        return scheduleWorkflow.deleteScheduleForTeamAtDay(teamId, dayId);
    }

    @RequestMapping("/getTeamScheduleForDay/{teamId}/{date}")
    public Day getTeamScheduleForDay(@PathVariable long teamId, @PathVariable @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate date) {
        return scheduleWorkflow.getTeamScheduleForDay(teamId, date);
    }

    @RequestMapping("/getmonthScheduleForTeam/{teamId}/{date}")
    public List<Day> getmonthScheduleForTeam(@PathVariable @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate date, @PathVariable long teamId) {
        return scheduleWorkflow.getmonthScheduleForTeam(date, teamId);
    }

    @RequestMapping("/getmonthSchedule/{date}")
    public List<Day> getmonthSchedule(@PathVariable @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate date) {
        return scheduleWorkflow.getmonthSchedule(date);
    }
}
