package com.codingSchool.TSAManager.dao;

import com.codingSchool.TSAManager.entities.Authorization;
import com.codingSchool.TSAManager.entities.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Service
public class UserDao {

    @Autowired
    private NamedParameterJdbcTemplate template;

    @Autowired
    private AuthorizationDao authorizationDao;

    public boolean addUser(User user, String role) {
        String q = "START TRANSACTION;"
                + " INSERT into TSAManager.user(username, password) values ( :username, :password)"
                + " INSERT into TSAManager.authorization (username, role) values ( :username, :role)"
                + " COMMIT;";
        BCryptPasswordEncoder bCrypt = new BCryptPasswordEncoder(11);
        MapSqlParameterSource param = new MapSqlParameterSource()
                .addValue("username", user.getUsername())
                .addValue("password", bCrypt.encode(user.getPassword()))
                .addValue("role", role);
        int i = template.update(q, param);
        return i > 0 ? true : false;
    }

    public boolean deleteUser(String userId) {
        String q = "delete TSAManager.user, TSAManager.authorization from TSAManager.user " +
                "inner join TSAManager.authorization on user.username = authorization.username " +
                "where user. idUser = :userId";
        MapSqlParameterSource param = new MapSqlParameterSource()
                .addValue("userId", userId);
        int i = template.update(q, param);
        return i > 0 ? true : false;
    }

    public List<User> getAllUsers() {
        String q = "select * from TSAManager.user";
        return template.query(q, this::userMapper);
    }

    private List<User> userMapper(ResultSet rs) throws SQLException {
        List<User> userList = new ArrayList<>();
        while (rs.next()) {
            User user = new User();
            user.setId(rs.getInt("idUser"));
            user.setUsername(rs.getString("username"));
            user.setAuthorization(authorizationDao.getUserRole(user.getUsername()));
        }
        return userList;
    }
}