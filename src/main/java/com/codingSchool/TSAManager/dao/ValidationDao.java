package com.codingSchool.TSAManager.dao;

import com.codingSchool.TSAManager.entities.Validation;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.UUID;

@Service
public class ValidationDao {
    private NamedParameterJdbcTemplate jdbcTemplate;

    public ValidationDao(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public String addValidation(Validation validation) {
        String q = "insert into validation (idvalidation, `date`) values (:id, :date)";
        validation.setIdvalidation(UUID.randomUUID().toString());
        MapSqlParameterSource param = new MapSqlParameterSource()
                .addValue("id", validation.getIdvalidation())
                .addValue("date", validation.getDate());

        KeyHolder key = new GeneratedKeyHolder();
        jdbcTemplate.update(q, param, key);

        return validation.getIdvalidation();
    }

    public LocalDate getValidationDate(String uuid, String certificateId) {
        String q = "SELECT validation.date from  certificate " +
                "inner join  validation on certificate.validation_idvalidation = validation.idvalidation " +
                "inner join employee on employee.idEmployee = certificate.employee_idEmployee " +
                "where certificate.employee_idEmployee = :id and certificate.certificate_list_idcertificate_list = :certificate";
        MapSqlParameterSource param = new MapSqlParameterSource()
                .addValue("id", uuid)
                .addValue("certificateId", certificateId);
        return jdbcTemplate.queryForObject(q, param, (rs, rowNum) ->{
           while (rs.next()){
               return rs.getDate("date").toLocalDate();
           }
            return null;
        });

    }


}
