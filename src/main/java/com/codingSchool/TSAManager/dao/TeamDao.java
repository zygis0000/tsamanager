package com.codingSchool.TSAManager.dao;

import com.codingSchool.TSAManager.entities.Team;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Service;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Service
public class TeamDao {
    @Autowired
    private EmployeeDao employeeDao;

    private NamedParameterJdbcTemplate jdbcTemplate;

    public TeamDao(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public long createTeam() {
        String q = "insert into team values ()";
        KeyHolder key = new GeneratedKeyHolder();
        jdbcTemplate.update(q, new MapSqlParameterSource(), key);
        return key.getKey().longValue();
    }

    public long deleteTeam(String teamId) {
        String q2 = "Update employee e set e.team_idTeam = 0 where e.team_idTeam = :id ";
        String q = "DELETE FROM `mydb`.`team` WHERE `idTeam` = :id";
        MapSqlParameterSource param = new MapSqlParameterSource()
                .addValue("id", teamId);
        KeyHolder key = new GeneratedKeyHolder();
        jdbcTemplate.update(q2, param);
        jdbcTemplate.update(q, param, key);
        return key.getKey().longValue();
    }

    public long addEmployeeToTeam(long teamID, String employeeId) {
        String q = "update employee set team_idTeam = :id where idEmployee = :employeeId";
        MapSqlParameterSource param = new MapSqlParameterSource()
                .addValue("id", teamID)
                .addValue("employeeId", employeeId);
        int i = jdbcTemplate.update(q, param);
        return i;

    }

    public List<Long> getAllTeamsIds() {
        String q = "select * from team ";
        List<Long> listIds = new ArrayList<>();
        return jdbcTemplate.query(q, rs -> {
            while (rs.next()) {
                listIds.add(rs.getLong("idTeam"));
            }
            return listIds;
        });
    }

    public int deleteEmployeeFromTeam(String employeeId) {
        String q = "update employee set employee.team_idTeam = 0 where idEmployee = :id";
        MapSqlParameterSource param = new MapSqlParameterSource()
                .addValue("id", employeeId);
        return jdbcTemplate.update(q, param);

    }

    public List<Team> getAllTeams() {
        List<Team> teams = new ArrayList<>();
        for (int i = 0; i < getAllTeamsIds().size(); i++) {
            String q = "select * from employee " +
                    "inner join team on team.idTeam = employee.team_idTeam " +
                    "where team.idTeam = " + getAllTeamsIds().get(i);
            teams.add(jdbcTemplate.query(q, this::teamMapper));
        }
        return teams;
    }

    public Team getTeam(long teamId) {
        String q = "select * from employee " +
                "inner join team on team.idTeam = employee.team_idTeam " +
                "where team.idTeam = :teamId";
        MapSqlParameterSource param = new MapSqlParameterSource()
                .addValue("teamId", teamId);
        return jdbcTemplate.query(q, param, this::teamMapper);
    }

    public int countTeams() {
        String q = "select count(idTeam) as `count` from team";
        return jdbcTemplate.queryForObject(q, new MapSqlParameterSource(), (rs, rowNum) -> rs.getInt("count"));
    }


    public int countEmployeesInTeam(String teamId) {
        String q = "select count(employee.idEmployee) as count from employee where employee.team_idTeam = :id";
        MapSqlParameterSource param = new MapSqlParameterSource()
                .addValue("id", teamId);
        return jdbcTemplate.queryForObject(q, param, (rs, rowNum) -> rs.getInt("count"));
    }


    private Team teamMapper(ResultSet rs) throws SQLException {
        Team team = new Team();
        while (rs.next()) {
            team.setIdTeam(rs.getLong("idTeam"));
            team.setEmployees(employeeDao.employeesMapper(rs));
        }
        return team;
    }


}
