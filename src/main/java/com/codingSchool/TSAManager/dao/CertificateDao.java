package com.codingSchool.TSAManager.dao;

import com.codingSchool.TSAManager.entities.Certificate;
import com.codingSchool.TSAManager.entities.Employee;
import com.codingSchool.TSAManager.entities.Validation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Service;


import java.sql.ResultSet;
import java.sql.SQLException;


@Service
public class CertificateDao {

    private NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    private CertificateListDAO certificateListDAO;

    public CertificateDao(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public Certificate getEmployeeCertificates(String id) {
        String q = "select  certificate_list.title, validation.date from certificate " +
                "inner join certificate_list on certificate_list.idcertificate_list =certificate.certificate_list_idcertificate_list " +
                "inner join validation on validation.idvalidation = certificate.validation_idvalidation " +
                "where certificate.employee_idEmployee = :id";
        MapSqlParameterSource param = new MapSqlParameterSource()
                .addValue("id", id);
        return jdbcTemplate.query(q, param, this::employeeCertificatesMapping);

    }

    public Certificate getEmployeeCertificate(String id, String certificateId) {
        String q = "select certificate_list.title, validation.date from certificate " +
                "inner join certificate_list on certificate_list.idcertificate_list =certificate.certificate_list_idcertificate_list " +
                "inner join validation on validation.idvalidation = certificate.validation_idvalidation " +
                "where certificate.employee_idEmployee = :id and certificate.certificate_list_idcertificate_list =:certificateId";
        MapSqlParameterSource param = new MapSqlParameterSource()
                .addValue("id", id)
                .addValue("certificateId", certificateId);
        return jdbcTemplate.query(q, param, this::employeeCertificateMapping);

    }

    public int addCertificateToEmployee(String id, String certificateId, Validation validation) {
        String q = "INSERT INTO certificate (employee_idEmployee, certificate_list_idcertificate_list, validation_idvalidation) "
                + "VALUES (:id, :certificateId, :valId)";
        MapSqlParameterSource param = new MapSqlParameterSource()
                .addValue("id", id)
                .addValue("certificateId", certificateId)
                .addValue("valId", validation.getIdvalidation());
        return jdbcTemplate.update(q, param);

    }

    public int deleteCertificateFromEmployee(String id, String certificateId) {
        String q = "delete certificate, validation from certificate " +
                "inner join validation on validation.idvalidation = certificate.validation_idvalidation " +
                "where certificate.employee_idEmployee = :id and certificate.certificate_list_idcertificate_list = :certificateId";
        MapSqlParameterSource param = new MapSqlParameterSource()
                .addValue("id", id)
                .addValue("certificateId", certificateId);
        return jdbcTemplate.update(q, param);

    }

    public int countEmployeeCertifcates(String employeeId) {
        String q = "select count(certificate_list_idcertificate_list) as count from certificate" +
                " where certificate.employee_idEmployee = :eid ";

        MapSqlParameterSource param = new MapSqlParameterSource()
                .addValue("eid", employeeId);

        return jdbcTemplate.query(q, param,  rs -> {
            while (rs.next()) {
                return rs.getInt("count");
            }
            return 0;
        });
    }

    private Certificate employeeCertificatesMapping(ResultSet rs) throws SQLException {
        Certificate certificate = new Certificate();
        while (rs.next()) {
            certificate.setCertificateList(certificateListDAO.certificatesMapping(rs));
        }
        return certificate;
    }

    private Certificate employeeCertificateMapping(ResultSet rs) throws SQLException {
        Certificate certificate = new Certificate();
        while (rs.next()) {
            certificate.setCertificate(certificateListDAO.certificateMapper(rs));

        }
        return certificate;
    }

}
