package com.codingSchool.TSAManager.dao;

import com.codingSchool.TSAManager.entities.Contacts;
import com.codingSchool.TSAManager.entities.Employee;
import com.codingSchool.TSAManager.entities.EmployeeBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Service;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class EmployeeDao {

    @Autowired
    private EmployeeBuilder empBuild;


    private NamedParameterJdbcTemplate jdbcTemplate;

    public EmployeeDao(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public String getPosition(String uuid) {
        String query = "select employee.position from employee where employee.idEmployee = :id";
        MapSqlParameterSource param = new MapSqlParameterSource()
                .addValue("id", uuid);
        String position = jdbcTemplate.queryForObject(query, param, (rs, rowNum) -> rs.getString("position"));
        return position;
    }

    public int changePosition(String uuid, String position) {
        String query = "update employee set employee.position = :position where employee.idEmployee = :id";
        MapSqlParameterSource param = new MapSqlParameterSource()
                .addValue("id", uuid)
                .addValue("position", position);
        int update = jdbcTemplate.update(query, param);

        return update;
    }

    public String addEmployee(Employee employee, Contacts contacts) {
        String q = "INSERT INTO employee (idEmployee, FirstName, LastName, gender, identityCode, `position`, address, town) " +
                "VALUES (:id, :firstname, :lastName, :gender, :identityCode, :`position`, :town )";
        UUID uuid = UUID.randomUUID();
        employee.setIdEmployee(uuid.toString());
        MapSqlParameterSource params = new MapSqlParameterSource()
                .addValue("id", employee.getIdEmployee())
                .addValue("firstName", employee.getFirstName())
                .addValue("lastName", employee.getLastName())
                .addValue("gender", employee.getGender())
                .addValue("identityCode", employee.getIdentityCode())
                .addValue("position", employee.getPosition())
                .addValue("town", employee.getTown());

        KeyHolder holder = new GeneratedKeyHolder();
        jdbcTemplate.update(q, params, holder);
        UUID id = (UUID) holder.getKeys().get("id");
        addEmployeeContacts(contacts, id.toString());

        return id.toString();
    }

    public int deleteEmployee(String uuid) {
        String query = "delete employee, contacts, certificate, `validation` from employee inner join contacts on contacts.employee_idEmployee = employee.idEmployee "
                + " inner join certificate on certificate.employee_idEmployee =employee.idEmployee "
                + "inner join `validation` on `validation`.idvalidation =certificate.validation_idvalidation where employee.idEmployee = :id";
        MapSqlParameterSource param = new MapSqlParameterSource()
                .addValue("id", uuid);
        int del = jdbcTemplate.update(query, param);

        return del;
    }


    public int addEmployeeContacts(Contacts contacts, String uuid) {
        String q = "INSERT INTO contacts (phoneNumber, email, employee_idEmployee) values (:phoneNumber, :email, :idEmployee)";
        MapSqlParameterSource params = new MapSqlParameterSource()
                .addValue("phoneNumber", contacts.getPhoneNumber())
                .addValue("email", contacts.getEmail())
                .addValue("idEmployee", uuid);
       return jdbcTemplate.update(q, params);
    }

    public List<Employee> getAllByGender(String gender) {
        String q = "SELECT * FROM employee WHERE gender = :gender";
        MapSqlParameterSource param = new MapSqlParameterSource()
                .addValue("gender", gender);
        return jdbcTemplate.query(q, param, this::employeesMapper);

    }

    public int countEmployeeByGender(String gender) {
        String q = "SELECT count(gender) as amount FROM employee WHERE gender = :gender";
        MapSqlParameterSource param = new MapSqlParameterSource()
                .addValue("gender", gender);
        return jdbcTemplate.queryForObject(q, param, (rs, rowNum) -> rs.getInt("amount"));

    }

    public String getEmployeeGender(String uuid) {
        String q = "SELECT gender FROM employee WHERE idEmployee = :id";
        MapSqlParameterSource param = new MapSqlParameterSource()
                .addValue("id", uuid);
        String gender = jdbcTemplate.queryForObject(q, param, (rs, rowNum) -> rs.getString("gender"));
        return gender;
    }

    public List<String> getIdsOfEmployeesByGenderWithoutTeam(String gender) {
        String q = "SELECT idEmployee FROM employee WHERE employee.gender = :gender and team_idTeam = 0";
        MapSqlParameterSource param = new MapSqlParameterSource()
                .addValue("gender", gender);
        return jdbcTemplate.query(q, param, rs -> {
            List<String> list = new ArrayList<>();
            while (rs.next()) {

                list.add(rs.getString("idEmployee"));
            }

            return list;
        });

    }

    public Employee getEmployeeById(String uuid) {
        String q = "SELECT * FROM employee WHERE idEmployee = :id";

        return jdbcTemplate.queryForObject(q, new MapSqlParameterSource().addValue("id", uuid), (rs, rowNum) -> employeeRowMapper(rs));
    }

    public List<String> getAllIdsOfEmployees() {
        String q = "SELECT idEmployee FROM employee";
        List<String> ids = jdbcTemplate.query(q, this::employeeIdMapper);

        return ids;
    }

    public List<Employee> getAllEmployees() {
        String q = "SELECT * FROM employee ";
        List<Employee> employeeList = jdbcTemplate.query(q, this::employeesMapper);

        return employeeList;
    }

    public List<Employee> employeesMapper(ResultSet rs) throws SQLException {
        List<Employee> employees = new ArrayList<>();
        while (rs.next()) {
            employees.add(employeeRowMapper(rs));
        }
        return employees;
    }
    private List<Contacts> getEmployeeContacts(String uuid){
        String q ="select c.phoneNumber, c.email from contacts c" +
                " inner join employee e on e.idEmployee = c.employee_idEmployee " +
                "where e.idEmployee = :uuid";
        MapSqlParameterSource param = new MapSqlParameterSource()
                .addValue("uuid", uuid);
       return jdbcTemplate.query(q, param, this::contactsRowMapper);
    }


    private Employee employeeRowMapper(ResultSet rs) throws SQLException {

            return empBuild.setId(rs.getString("idEmployee"))
                    .setFirstName(rs.getString("FirstName"))
                    .setLastName(rs.getString("LastName"))
                    .setGender(rs.getString("gender"))
                    .setIdentityCode(rs.getString("identityCode"))
                    .setPosition(rs.getString("position"))
                    .setAddress(rs.getString("address"))
                    .setTown(rs.getString("town"))
                    .setContacts(getEmployeeContacts(rs.getString("idEmployee")))
                    .build();

    }

    private List<Contacts> contactsRowMapper(ResultSet rs) throws SQLException {
        List<Contacts> contactsList = new ArrayList<>();
        while (rs.next()) {
            Contacts contacts = new Contacts();
            contacts.setEmail(rs.getString("email"));
            contacts.setPhoneNumber(rs.getString("phoneNumber"));
            contactsList.add(contacts);
        }
        return contactsList;
    }

    private List<String> employeeIdMapper(ResultSet rs) throws SQLException {

        List<String> ids = new ArrayList<>();
        while (rs.next()) {
            ids.add(rs.getString("idEmployee"));
        }
        return ids;

    }


}
