package com.codingSchool.TSAManager.dao;

import com.codingSchool.TSAManager.entities.Contacts;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Service;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


@Service
public class ContactsDao {

    private NamedParameterJdbcTemplate jdbcTemplate;

    public ContactsDao(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public List<Contacts> getEmployeeContacts(String id) {
        String q = "select contacts.phoneNumber, contacts.email from employee, contacts where employee.idEmployee = contacts.employee_idEmployee and employee.idEmployee = :id ";
        MapSqlParameterSource param = new MapSqlParameterSource()
                .addValue("id", id);
        List<Contacts> contactsList = jdbcTemplate.queryForObject(q, param, (rs, rowNum) -> contactsMapper(rs));

        return contactsList;
    }

    private List<Contacts> contactsMapper(ResultSet rs) throws SQLException {
        List<Contacts> contactsList = new ArrayList<>();
        while (rs.next()) {
            Contacts contacts = new Contacts();
            contacts.setPhoneNumber(rs.getString("phoneNumber"));
            contacts.setEmail(rs.getString("email"));
            contactsList.add(contacts);
        }
        return contactsList;
    }
}
