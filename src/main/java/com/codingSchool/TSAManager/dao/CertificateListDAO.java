package com.codingSchool.TSAManager.dao;


import com.codingSchool.TSAManager.entities.CertificateList;
import com.codingSchool.TSAManager.entities.Validation;
import org.springframework.jdbc.core.ResultSetExtractor;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Service;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class CertificateListDAO {

    private NamedParameterJdbcTemplate jdbcTemplate;

    public CertificateListDAO(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public String addCertification(CertificateList certificate) {
        UUID id = UUID.randomUUID();
        certificate.setIdCertificateList(id.toString());
        MapSqlParameterSource param = new MapSqlParameterSource()
                .addValue("id", certificate.getIdCertificateList())
                .addValue("title", certificate.getTitle());
        String q = "insert into certificate_list( idcertificate_list, title) values(:id, :title)";
        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(q, param);
        UUID uuid = (UUID) keyHolder.getKeys().get("id");
        return uuid.toString();
    }

    public CertificateList getCertificate(String id) {
        String q = "select * from certificate_list where certificate_list.idcertificate_list = :id";
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource()
                .addValue("id", id);
        CertificateList object = jdbcTemplate.query(q, mapSqlParameterSource,(ResultSetExtractor<CertificateList>) rs -> certificateMapper(rs));
        return object;
    }

    public List<CertificateList> getAllCertificates() {
        String q = "select * from certificate_list";
        List<CertificateList> certificateList = jdbcTemplate.query(q, this::certificatesMapping);
        return certificateList;
    }

    public int deleteCertificate(String uuid) {
        String q = "Delete from certificate_list where idcertificate_list = :id";
        MapSqlParameterSource param = new MapSqlParameterSource()
                .addValue("id", uuid);
        int a = jdbcTemplate.update(q, param);
        return a;
    }

    public int updateSertificate(String uuid, String title) {
        String q = "update certificate_list set certificate_list.title = :title where certificate_list.idcertificate_list = :id";
        MapSqlParameterSource param = new MapSqlParameterSource()
                .addValue("id", uuid)
                .addValue("title", title);
        int a = jdbcTemplate.update(q, param);
        return a;
    }
    private boolean resultSetColumnCheck(ResultSet rs, String columnName) throws SQLException {
        ResultSetMetaData meta = rs.getMetaData();
        int columns = meta.getColumnCount();
        for (int i = 1; i <= columns; i++) {
            if (columnName.equals(meta.getColumnName(i))) {
                return true;
            }
        }
        return false;
    }

    public List<CertificateList> certificatesMapping(ResultSet rs) throws SQLException {
        List<CertificateList> list = new ArrayList<>();
        String id ="idcertificate_list";
        while (rs.next()) {
            CertificateList certificateList = new CertificateList();
            mapper(rs, certificateList, id);
            list.add(certificateList);
        }
        return list;
    }

    public CertificateList certificateMapper(ResultSet rs) throws SQLException {
        CertificateList certificateList = new CertificateList();
        String id ="idcertificate_list";
        while (rs.next()) {
            mapper(rs, certificateList, id);
        }
        return certificateList;
    }

    private void mapper(ResultSet rs, CertificateList certificateList, String id) throws SQLException {
        if (resultSetColumnCheck(rs,id)) {
            certificateList.setIdCertificateList(rs.getString("idcertificate_list"));
        }
        certificateList.setTitle(rs.getString("title"));
        String column = "date";
        if (resultSetColumnCheck(rs, column)){
            Validation validation = new Validation();
            validation.setDate(rs.getDate("date").toLocalDate());
            certificateList.setValidation(validation);
        }
    }
}
