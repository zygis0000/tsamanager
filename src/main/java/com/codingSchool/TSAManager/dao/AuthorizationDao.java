package com.codingSchool.TSAManager.dao;

import com.codingSchool.TSAManager.entities.Authorization;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Service;

import java.sql.ResultSet;
import java.sql.SQLException;

@Service
public class AuthorizationDao {

    @Autowired
    private NamedParameterJdbcTemplate template;

    public Authorization getUserRole(String username) {
        String q = "select username role from TSAManager.authorization "
                 + "where username = :username";
        MapSqlParameterSource param = new MapSqlParameterSource()
                .addValue("username", username);
        return template.query(q, param, this::mapUserAuthorization);
    }

    private Authorization mapUserAuthorization(ResultSet rs) throws SQLException {
        while (rs.next()) {
            Authorization auth = new Authorization();
            auth.setUsername(rs.getString("username"));
            auth.setRole(rs.getString("role"));
            return auth;
        }
        return null;
    }
}
