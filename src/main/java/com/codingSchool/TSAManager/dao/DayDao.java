package com.codingSchool.TSAManager.dao;

import com.codingSchool.TSAManager.entities.Day;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Service;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Service
public class DayDao {

    private NamedParameterJdbcTemplate jdbcTemplate;

    public DayDao(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public long createDay(Day day) {
        String q = "insert into `day`(`date`, shift, flag) values (:date, :shift, :flag)";
        MapSqlParameterSource param = new MapSqlParameterSource()
                .addValue("date", day.getDate())
                .addValue("shift", day.getShift())
                .addValue("flag", day.getFlag());

        KeyHolder key = new GeneratedKeyHolder();
        jdbcTemplate.update(q, param, key);

        return key.getKey().longValue();
    }

    public LocalDate getDate(long id) {
        String q = "SELECT `day`.date from `day` where `day`.idday = :id ";
        MapSqlParameterSource param = new MapSqlParameterSource()
                .addValue("id", id);
        return jdbcTemplate.query(q, param, (ResultSetExtractor<LocalDate>) rs -> rs.getDate("date").toLocalDate());
    }

    //    public boolean checkOrExists(Date date){
//        String q ="";
//        MapSqlParameterSource param = new MapSqlParameterSource()
//                .addValue("id", id);
//        return jdbcTemplate.queryForObject(q, param, (rs, rowNum) -> rs.getDate("date"));
//    }
    public Day getDay(long id) {
        String q = "select * from `day` where `day`.idday = :id";
        MapSqlParameterSource param = new MapSqlParameterSource()
                .addValue("id", id);
        return jdbcTemplate.query(q, param, this::mappingDay);
    }

    public String checkShift(long id) {
        String q = "select `day`.shift from `day` where `day`.idday = :id";
        MapSqlParameterSource param = new MapSqlParameterSource()
                .addValue("id", id);
        return jdbcTemplate.query(q, param, (ResultSetExtractor<String>) rs -> rs.getString("shift"));
    }

    public String checkFlag(long id) {
        String q = "select `day`.flag from `day` where `day`.idday = :id";
        MapSqlParameterSource param = new MapSqlParameterSource()
                .addValue("id", id);
        return jdbcTemplate.query(q, param, (ResultSetExtractor<String>) rs -> rs.getString("flag"));
    }

    public int deleteDay(long id) {
        String q = "delete schedule, `day` from `day`" +
                " inner join schedule on schedule.dayId = `day`.idday " +
                "where `day`.idday = :id";
        MapSqlParameterSource param = new MapSqlParameterSource()
                .addValue("id", id);
        int i = jdbcTemplate.update(q, param);

        return i;
    }

    public String checkbyDateAndTeamIdShift(LocalDate date, long teamId) {
        String q = "select d.shift from `day` d" +
                " inner join schedule s on s.dayId = d.idday" +
                " where d.`date` = :date and s.teamId = :idTeam";
        MapSqlParameterSource param = new MapSqlParameterSource()
                .addValue("date", date.toString())
                .addValue("idTeam", teamId);
        return jdbcTemplate.query(q, param, rs -> {
            while (rs.next()) {
                return rs.getString("shift");
            }
            return null;
        });

    }

    public String checkbyDateAndTeamIdFlag(LocalDate date, long teamId) {
        String q = "select d.flag from `day` d" +
                " inner join schedule s on s.dayId = d.idday" +
                " where d.`date` = :date and s.teamId = :idTeam";
        MapSqlParameterSource param = new MapSqlParameterSource()
                .addValue("date", date.toString())
                .addValue("idTeam", teamId);
        return jdbcTemplate.query(q, param, rs -> {
            while (rs.next()) {
                return rs.getString("flag");
            }
            return null;
        });

    }

    public List<Day> getByYearAndMonthShift(LocalDate date, long teamid) {
        String q = "select `day`.`date`, `day`.shift, `day`.flag" +
                " from `day`" +
                "inner join schedule s on day.idday = s.dayId" +
                " where YEAR(`day`.`date`) = :year and month(`day`.`date`) = :month and s.teamId = :teamid";

        MapSqlParameterSource param = new MapSqlParameterSource()
                .addValue("year", date.getYear())
                .addValue("month", date.getMonthValue())
                .addValue("teamid", teamid);
        return jdbcTemplate.queryForObject(q, param, (rs, rowNum) -> mappingDays(rs));
    }


    public Day mappingDay(ResultSet rs) throws SQLException {
        Day day = new Day();
        while (rs.next()) {
            day.setDate(rs.getDate("date").toLocalDate());
            day.setFlag(rs.getString("flag"));
            day.setShift(rs.getString("shift"));
            return day;
        }
        return null;
    }

    public List<Day> mappingDays(ResultSet rs) throws SQLException {
        List<Day> days = new ArrayList<>();
        while (rs.next()) {
            Day day = new Day();
            day.setDate(rs.getDate("date").toLocalDate());
            day.setFlag(rs.getString("flag"));
            day.setShift(rs.getString("shift"));
            days.add(day);
        }
        return days;
    }
}
