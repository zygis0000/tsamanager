package com.codingSchool.TSAManager.dao;

import com.codingSchool.TSAManager.entities.Day;
import com.codingSchool.TSAManager.entities.Schedule;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;


@Service
public class ScheduleDao {
    private NamedParameterJdbcTemplate jdbcTemplate;
    @Autowired
    private DayDao dayDao;

    public ScheduleDao(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public long createScheduleForTeamAtDay(Schedule schedule) {
        String q = "INSERT into schedule (teamId, dayId) values (:teamId, :dayId)";
        MapSqlParameterSource param = new MapSqlParameterSource()
                .addValue("teamId", schedule.getTeamId())
                .addValue("dayId", schedule.getDayId());
        KeyHolder key = new GeneratedKeyHolder();
        jdbcTemplate.update(q, param, key);

        return key.getKey().longValue();

    }

    public long deleteScheduleForTeamAtDay(long teamId, long dayId) {
        String q = "delete schedule, `day` from schedule" +
                " inner join `day` on `day`.idday = schedule.dayId " +
                "where `day`.idday = :dayId and schedule.teamId = :teamId";
        MapSqlParameterSource param = new MapSqlParameterSource()
                .addValue("teamId", teamId)
                .addValue("dayId", dayId);
        KeyHolder key = new GeneratedKeyHolder();
        jdbcTemplate.update(q, param, key);

        return key.getKey().longValue();
    }

    public Day getTeamScheduleForDay(long teamid, LocalDate date) {
        String q = "select `day`.date, `day`.shift, `day`.flag  from `day` " +
                "inner join schedule on schedule.dayId = `Day`.idday " +
                "where schedule.teamId = :id and `day`.`date` = :date";
        MapSqlParameterSource param = new MapSqlParameterSource()
                .addValue("id", teamid)
                .addValue("date", date.toString());
        return jdbcTemplate.query(q, param, (ResultSetExtractor<Day>) (rs) -> dayDao.mappingDay(rs));
    }

    public List<Day> getmonthScheduleForTeam(LocalDate date, long teamId) {
        String q = "select  `day`.date, `day`.shift, `day`.flag from schedule " +
                "inner join `day` on `day`.idday = schedule.dayId " +
                "where schedule.teamId= :teamId and" +
                " YEAR (`day`.date) = :year and  MONTH (`day`.date) = :month";
        MapSqlParameterSource param = new MapSqlParameterSource()
                .addValue("teamId", teamId)
                .addValue("year", date.getYear())
                .addValue("month", date.getMonthValue());
        return jdbcTemplate.queryForObject(q, param, (rs, rowNum) -> dayDao.mappingDays(rs));

    }

    public List<Day> getmonthSchedule(LocalDate date) {
        String q = "select  `day`.date, `day`.shift, `day`.flag from schedule " +
                "inner join `day` on `day`.idday = schedule.dayId " +
                "where YEAR (`day`.date) = :year and  MONTH (`day`.date) = :month";
        MapSqlParameterSource param = new MapSqlParameterSource()
                .addValue("year", date.getYear() )
                .addValue("month", date.getMonthValue());
        return jdbcTemplate.query(q, param, (ResultSetExtractor<List<Day>>) rs -> dayDao.mappingDays(rs));

    }

}
