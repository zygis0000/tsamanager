package com.codingSchool.TSAManager.security.authorization.userDetailServices;

import com.codingSchool.TSAManager.entities.Authorization;
import com.codingSchool.TSAManager.entities.User;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.*;

public class UserPrincipals implements UserDetails {

    private User user;
    private List<Authorization> authorizations;

    public UserPrincipals(User user, List<Authorization> authorizations) {
        super();
        this.user = user;
        this.authorizations = authorizations;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        if (authorizations == null) {
            return Collections.emptySet();
        }

        Set<SimpleGrantedAuthority> authorities = new HashSet<>();
        authorizations.forEach(auth -> {
            authorities.add(new SimpleGrantedAuthority(auth.getRole()));
        });

        return authorities;

    }

    @Override
    public String getPassword() {
        return this.user.getPassword();
    }

    @Override
    public String getUsername() {
        return this.user.getUsername();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
