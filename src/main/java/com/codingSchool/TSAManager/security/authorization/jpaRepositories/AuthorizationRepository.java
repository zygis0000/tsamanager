package com.codingSchool.TSAManager.security.authorization.jpaRepositories;

import com.codingSchool.TSAManager.entities.Authorization;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AuthorizationRepository extends JpaRepository<Authorization, Long> {
    List<Authorization> findByUsername(String username);
}
