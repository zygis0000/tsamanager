package com.codingSchool.TSAManager.security.authorization.userDetailServices;

import com.codingSchool.TSAManager.security.authorization.jpaRepositories.AuthorizationRepository;
import com.codingSchool.TSAManager.security.authorization.jpaRepositories.UserRepository;
import com.codingSchool.TSAManager.entities.Authorization;
import com.codingSchool.TSAManager.entities.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserDetailsServices implements UserDetailsService {

    private final UserRepository userRepository;
    private final AuthorizationRepository authGroupRepository;

    public UserDetailsServices(UserRepository userRepository, AuthorizationRepository authGroupRepository) {
        super();
        this.userRepository = userRepository;
        this.authGroupRepository = authGroupRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = this.userRepository.findByUsername(username);
        if (user == null) {
            throw new UsernameNotFoundException("Cannot find username: " + username);
        }
        List<Authorization> authorizations = this.authGroupRepository.findByUsername(username);
        return new UserPrincipals(user, authorizations);
    }
}
