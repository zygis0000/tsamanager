package com.codingSchool.TSAManager.migrations;

import org.flywaydb.core.api.migration.spring.SpringJdbcMigration;
import org.springframework.jdbc.core.JdbcTemplate;

public class V1_0_3__CreateCertificateListTable implements SpringJdbcMigration {
    @Override
    public void migrate(JdbcTemplate jdbcTemplate) throws Exception {
        String q = "create table if not exists TSAManager.certificate_list " +
                "(" +
                " idcertificate_list varchar(100) not null " +
                " primary key, " +
                " title varchar(100) not null " +
                " );";
        jdbcTemplate.execute(q);
    }
}
