package com.codingSchool.TSAManager.migrations;

import org.flywaydb.core.api.migration.spring.SpringJdbcMigration;
import org.springframework.jdbc.core.JdbcTemplate;

public class V1_0_2__CreateContactsTable implements SpringJdbcMigration {
    @Override
    public void migrate(JdbcTemplate jdbcTemplate) throws Exception {
        String q = "create table if not exists TSAManager.contacts " +
                "(" +
                " idcontacts int auto_increment, " +
                " phoneNumber varchar(45) null, " +
                " email varchar(45) null, " +
                " employee_idEmployee varchar(100) not null, " +
                " primary key (idcontacts, employee_idEmployee), " +
                " constraint idcontacts_UNIQUE " +
                " unique (idcontacts), " +
                " constraint fk_contacts_employee1 " +
                " foreign key (employee_idEmployee) references TSAManager.employee (idEmployee) " +
                " ); ";
        jdbcTemplate.execute(q);
    }
}
