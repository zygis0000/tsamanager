package com.codingSchool.TSAManager.migrations;

import org.flywaydb.core.api.migration.spring.SpringJdbcMigration;
import org.springframework.jdbc.core.JdbcTemplate;

public class V1_0_4__CreateValidationTable implements SpringJdbcMigration {
    @Override
    public void migrate(JdbcTemplate jdbcTemplate) throws Exception {
        String q = "create table if not exists TSAManager.validation " +
                "( " +
                " idvalidation varchar(100) not null, " +
                " date date not null, " +
                " primary key (idvalidation), " +
                " constraint idvalidation_UNIQUE " +
                " unique (idvalidation) " +
                ");";
        jdbcTemplate.execute(q);
    }
}
