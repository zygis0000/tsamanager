package com.codingSchool.TSAManager.migrations;

import org.flywaydb.core.api.migration.spring.SpringJdbcMigration;
import org.springframework.jdbc.core.JdbcTemplate;

public class V1_1_0__CreateScheduleTable implements SpringJdbcMigration {
    @Override
    public void migrate(JdbcTemplate jdbcTemplate) throws Exception {
        String q = "create table if not exists TSAManager.schedule " +
                "( " +
                " idschedule bigint auto_increment, " +
                " teamId bigint not null, " +
                " dayId bigint not null, " +
                " primary key (idschedule, teamId), " +
                " constraint dateId_UNIQUE " +
                " unique (dayId), " +
                " constraint fk_schedule_team1 " +
                " foreign key (teamId) references TSAManager.team (idTeam) " +
                ");";
        jdbcTemplate.execute(q);
    }
}
