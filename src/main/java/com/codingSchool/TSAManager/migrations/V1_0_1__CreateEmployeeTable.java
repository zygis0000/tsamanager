package com.codingSchool.TSAManager.migrations;

import org.flywaydb.core.api.migration.spring.SpringJdbcMigration;
import org.springframework.jdbc.core.JdbcTemplate;

@SuppressWarnings("unused")
public class V1_0_1__CreateEmployeeTable implements SpringJdbcMigration {
    @Override
    public void migrate(JdbcTemplate jdbcTemplate) throws Exception {
        String q = "create table if not exists TSAManager.employee " +
                "(" +
                " idEmployee varchar(100) not null," +
                " FirstName varchar(45) not null, " +
                " LastName varchar(45) not null, " +
                " gender varchar(45) not null, " +
                " identityCode varchar(45) not null, " +
                " position varchar(45) not null, " +
                " address varchar(45) not null, " +
                " town varchar(45) not null, " +
                " team_idTeam bigint default 0 null, " +
                " primary key (idEmployee)," +
                " constraint idEmployee_UNIQUE " +
                " unique (idEmployee) " +
                ");";

        jdbcTemplate.execute(q);
    }
}
