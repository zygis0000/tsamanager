package com.codingSchool.TSAManager.migrations;

import org.flywaydb.core.api.migration.spring.SpringJdbcMigration;
import org.springframework.jdbc.core.JdbcTemplate;

public class V1_0_8__CreateAuthorizationTable implements SpringJdbcMigration {
    @Override
    public void migrate(JdbcTemplate jdbcTemplate) throws Exception {
        String q = "create table if not exists TSAManager.authorization " +
                "( " +
                " idAuth bigint auto_increment " +
                " primary key, " +
                " role varchar(255) null, " +
                " username varchar(255) null " +
                ");";
        jdbcTemplate.execute(q);
    }
}
