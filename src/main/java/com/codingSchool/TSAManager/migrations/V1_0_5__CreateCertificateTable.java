package com.codingSchool.TSAManager.migrations;

import org.flywaydb.core.api.migration.spring.SpringJdbcMigration;
import org.springframework.jdbc.core.JdbcTemplate;

public class V1_0_5__CreateCertificateTable implements SpringJdbcMigration {
    @Override
    public void migrate(JdbcTemplate jdbcTemplate) throws Exception {
        String q = "create table if not exists TSAManager.certificate " +
                "(" +
                " certificateId bigint auto_increment," +
                " employee_idEmployee varchar(100) not null," +
                " certificate_list_idcertificate_list varchar(100) not null," +
                " validation_idvalidation varchar(100) not null," +
                " primary key (certificateId, certificate_list_idcertificate_list, validation_idvalidation, employee_idEmployee)," +
                " constraint certificateId_UNIQUE" +
                " unique (certificateId)," +
                " constraint fk_certificate_certificate_list1" +
                " foreign key (certificate_list_idcertificate_list) references TSAManager.certificate_list (idcertificate_list)," +
                " constraint fk_certificate_employee1" +
                " foreign key (employee_idEmployee) references TSAManager.employee (idEmployee)," +
                " constraint fk_certificate_validation1" +
                " foreign key (validation_idvalidation) references TSAManager.validation (idvalidation) " +
                ");";
        jdbcTemplate.execute(q);
    }
}
