package com.codingSchool.TSAManager.migrations;

import org.flywaydb.core.api.migration.spring.SpringJdbcMigration;
import org.springframework.jdbc.core.JdbcTemplate;

public class V1_0_9__CreateUserTable implements SpringJdbcMigration {

    @Override
    public void migrate(JdbcTemplate jdbcTemplate) throws Exception {
        String q ="create table if not exists TSAManager.user " +
                "( " +
                " idUser bigint auto_increment " +
                " primary key, " +
                " password varchar(255) null, " +
                " username varchar(255) not null " +
                ");";
             jdbcTemplate.execute(q);
    }
}
