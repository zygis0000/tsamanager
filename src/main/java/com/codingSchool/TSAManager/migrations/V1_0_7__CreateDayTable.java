package com.codingSchool.TSAManager.migrations;

import org.flywaydb.core.api.migration.spring.SpringJdbcMigration;
import org.springframework.jdbc.core.JdbcTemplate;

public class V1_0_7__CreateDayTable implements SpringJdbcMigration {
    @Override
    public void migrate(JdbcTemplate jdbcTemplate) throws Exception {
        String q = "create table if not exists TSAManager.day " +
                "( " +
                " idday bigint auto_increment, " +
                " date date not null, " +
                " shift enum('d', 'n', 'h') not null, " +
                " flag enum('1', '2', '3', ' 4') not null, " +
                " primary key (idday), " +
                " constraint iddate_UNIQUE " +
                " unique (idday) " +
                ");";
        jdbcTemplate.execute(q);
    }
}
