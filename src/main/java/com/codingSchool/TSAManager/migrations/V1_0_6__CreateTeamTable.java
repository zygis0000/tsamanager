package com.codingSchool.TSAManager.migrations;

import org.flywaydb.core.api.migration.spring.SpringJdbcMigration;
import org.springframework.jdbc.core.JdbcTemplate;

public class V1_0_6__CreateTeamTable implements SpringJdbcMigration {
    @Override
    public void migrate(JdbcTemplate jdbcTemplate) throws Exception {
        String q = "create table if not exists TSAManager.team " +
                "( " +
                " idTeam bigint auto_increment, " +
                " primary key (idTeam), " +
                " constraint idshift_UNIQUE " +
                " unique (idTeam) " +
                ");";
        jdbcTemplate.execute(q);
    }
}
