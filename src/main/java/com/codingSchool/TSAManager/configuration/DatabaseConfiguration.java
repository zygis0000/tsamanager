package com.codingSchool.TSAManager.configuration;

import com.codingSchool.TSAManager.services.PropertyService;
import org.flywaydb.core.Flyway;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.context.annotation.*;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;

import javax.sql.DataSource;
import javax.persistence.EntityManagerFactory;

@Configuration
@PropertySource("classpath:database.properties")
@ComponentScan("com.codingSchool.TSAManager")
public class DatabaseConfiguration {

    @Autowired
    private Environment environment;

    private final PropertyService propertyService;

    @Autowired
    public DatabaseConfiguration(PropertyService propertyService) {
        this.propertyService = propertyService;
    }

    @Bean(initMethod = "migrate")
    Flyway flyway() {
        Flyway flyway = new Flyway();
        flyway.setBaselineOnMigrate(true);
        flyway.setLocations(propertyService.getDbConfigFile().getProperty("database.migrations.folder"));
        flyway.setDataSource(mysqlDataSource());
        return flyway;
    }

    @Bean
    @DependsOn("flyway")
    EntityManagerFactory entityManagerFactory() {
        LocalContainerEntityManagerFactoryBean bean = new LocalContainerEntityManagerFactoryBean();
        bean.setDataSource(mysqlDataSource());
        // other configurations
        return bean.getObject();
    }


    @Bean
    public DataSource mysqlDataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(environment.getProperty("database.connection.driver"));
        dataSource.setUrl(environment.getProperty("database.connection.url"));
        dataSource.setUsername(environment.getProperty("database.connection.username"));
        dataSource.setPassword(environment.getProperty("database.connection.password"));

        return dataSource;
    }

    @Bean
    public NamedParameterJdbcTemplate namedParameterJdbcTemplate(DataSource dataSource) {
        return new NamedParameterJdbcTemplate(dataSource);
    }
}
