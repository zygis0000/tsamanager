package com.codingSchool.TSAManager.entities;


import java.util.List;
import java.util.Objects;

public class Team {

  private long idTeam;
  private List<Employee> employees;

  public List<Employee> getEmployees() {
    return employees;
  }

  public void setEmployees(List<Employee> employees) {
    this.employees = employees;
  }

  public long getIdTeam() {
    return idTeam;
  }

  public void setIdTeam(long idTeam) {
    this.idTeam = idTeam;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Team team = (Team) o;
    return idTeam == team.idTeam &&
            Objects.equals(employees, team.employees);
  }

  @Override
  public int hashCode() {
    return Objects.hash(idTeam, employees);
  }
}
