package com.codingSchool.TSAManager.entities;


import java.util.Objects;

public class CertificateList {

  private String idCertificateList;
  private String title;
  private Validation validation;


  public String getIdCertificateList() {
    return idCertificateList;
  }

  public void setIdCertificateList(String idCertificateList) {
    this.idCertificateList = idCertificateList;
  }


  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public Validation getValidation() {
    return validation;
  }

  public void setValidation(Validation validation) {
    this.validation = validation;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    CertificateList that = (CertificateList) o;
    return Objects.equals(idCertificateList, that.idCertificateList) &&
            Objects.equals(title, that.title);
  }

  @Override
  public int hashCode() {
    return Objects.hash(idCertificateList, title);
  }
}
