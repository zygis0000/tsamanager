package com.codingSchool.TSAManager.entities;


import java.util.List;
import java.util.Objects;

public class Employee {

  private String idEmployee;
  private String firstName;
  private String lastName;
  private String gender;
  private String identityCode;
  private String position;
  private String address;
  private String town;
  private long teamIdTeam;
  private List<Certificate> certificateList;
  private List<Contacts> contacts;

  public List<Contacts> getContacts() {
    return contacts;
  }

  public void setContacts(List<Contacts> contacts) {
    this.contacts = contacts;
  }

  public List<Certificate> getCertificateList() {
    return certificateList;
  }

  public void setCertificateList(List<Certificate> certificateList) {
    this.certificateList = certificateList;
  }

  public String getIdEmployee() {
    return idEmployee;
  }

  public void setIdEmployee(String idEmployee) {
    this.idEmployee = idEmployee;
  }


  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }


  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }


  public String getGender() {
    return gender;
  }

  public void setGender(String gender) {
    this.gender = gender;
  }


  public String getIdentityCode() {
    return identityCode;
  }

  public void setIdentityCode(String identityCode) {
    this.identityCode = identityCode;
  }


  public String getPosition() {
    return position;
  }

  public void setPosition(String position) {
    this.position = position;
  }


  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }


  public String getTown() {
    return town;
  }

  public void setTown(String town) {
    this.town = town;
  }


  public long getTeamIdTeam() {
    return teamIdTeam;
  }

  public void setTeamIdTeam(long teamIdTeam) {
    this.teamIdTeam = teamIdTeam;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Employee employee = (Employee) o;
    return teamIdTeam == employee.teamIdTeam &&
            Objects.equals(idEmployee, employee.idEmployee) &&
            Objects.equals(firstName, employee.firstName) &&
            Objects.equals(lastName, employee.lastName) &&
            Objects.equals(gender, employee.gender) &&
            Objects.equals(identityCode, employee.identityCode) &&
            Objects.equals(position, employee.position) &&
            Objects.equals(address, employee.address) &&
            Objects.equals(town, employee.town) &&
            Objects.equals(certificateList, employee.certificateList);
  }

  @Override
  public int hashCode() {
    return Objects.hash(idEmployee, firstName, lastName, gender, identityCode, position, address, town, teamIdTeam, certificateList);
  }
}
