package com.codingSchool.TSAManager.entities;


import java.time.LocalDate;
import java.util.Objects;

public class Validation {

  private String idvalidation;
  private LocalDate date;


  public String getIdvalidation() {
    return idvalidation;
  }

  public void setIdvalidation(String idvalidation) {
    this.idvalidation = idvalidation;
  }


  public String getDate() {
    return date.toString();
  }

  public void setDate(LocalDate date) {
    this.date = date;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Validation that = (Validation) o;
    return Objects.equals(idvalidation, that.idvalidation) &&
            Objects.equals(date, that.date);
  }

  @Override
  public int hashCode() {
    return Objects.hash(idvalidation, date);
  }
}
