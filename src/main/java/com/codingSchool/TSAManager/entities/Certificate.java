package com.codingSchool.TSAManager.entities;

import java.util.List;
import java.util.Objects;

public class Certificate {
  private long certificateId;
  private String employeeIdEmployee;
  private String certificateListIdCertificateList;
  private String validationIdvalidation;

  private List<CertificateList> certificateList;
  private CertificateList certificate;


  public CertificateList getCertificate() {
    return certificate;
  }

  public void setCertificate(CertificateList certificate) {
    this.certificate = certificate;
  }

  public List<CertificateList> getCertificateList() {
    return certificateList;
  }

  public void setCertificateList(List<CertificateList> certificateList) {
    this.certificateList = certificateList;
  }



  public String getEmployeeIdEmployee() {
    return employeeIdEmployee;
  }

  public void setEmployeeIdEmployee(String employeeIdEmployee) {
    this.employeeIdEmployee = employeeIdEmployee;
  }


  public String getCertificateListIdCertificateList() {
    return certificateListIdCertificateList;
  }

  public void setCertificateListIdCertificateList(String certificateListIdCertificateList) {
    this.certificateListIdCertificateList = certificateListIdCertificateList;
  }


  public String getValidationIdvalidation() {
    return validationIdvalidation;
  }

  public void setValidationIdvalidation(String validationIdvalidation) {
    this.validationIdvalidation = validationIdvalidation;
  }

  public long getCertificateId() {
    return certificateId;
  }

  public void setCertificateId(long certificateId) {
    this.certificateId = certificateId;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Certificate that = (Certificate) o;
    return Objects.equals(employeeIdEmployee, that.employeeIdEmployee) &&
            Objects.equals(certificateListIdCertificateList, that.certificateListIdCertificateList) &&
            Objects.equals(validationIdvalidation, that.validationIdvalidation);
  }

  @Override
  public int hashCode() {
    return Objects.hash(employeeIdEmployee, certificateListIdCertificateList, validationIdvalidation);
  }
}
