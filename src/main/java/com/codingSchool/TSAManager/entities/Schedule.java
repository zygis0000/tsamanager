package com.codingSchool.TSAManager.entities;


import java.util.List;
import java.util.Objects;

public class Schedule {

  private long idschedule;
  private long teamId;
  private long dayId;
  private List<Day> days;
  private List<Team> teams;

  public List<Day> getDays() {
    return days;
  }

  public void setDays(List<Day> days) {
    this.days = days;
  }

  public List<Team> getTeams() {
    return teams;
  }

  public void setTeams(List<Team> teams) {
    this.teams = teams;
  }

  public long getIdschedule() {
    return idschedule;
  }

  public void setIdschedule(long idschedule) {
    this.idschedule = idschedule;
  }


  public long getTeamId() {
    return teamId;
  }

  public void setTeamId(long teamId) {
    this.teamId = teamId;
  }


  public long getDayId() {
    return dayId;
  }

  public void setDayId(long dayId) {
    this.dayId = dayId;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Schedule schedule = (Schedule) o;
    return idschedule == schedule.idschedule &&
            teamId == schedule.teamId &&
            dayId == schedule.dayId &&
            Objects.equals(days, schedule.days) &&
            Objects.equals(teams, schedule.teams);
  }

  @Override
  public int hashCode() {
    return Objects.hash(idschedule, teamId, dayId, days, teams);
  }
}
