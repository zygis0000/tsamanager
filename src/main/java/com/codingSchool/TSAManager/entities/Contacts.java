package com.codingSchool.TSAManager.entities;


import java.util.Objects;

public class Contacts {

  private long idcontacts;
  private String phoneNumber;
  private String email;
  private String employeeIdEmployee;


  public long getIdcontacts() {
    return idcontacts;
  }

  public void setIdcontacts(long idcontacts) {
    this.idcontacts = idcontacts;
  }


  public String getPhoneNumber() {
    return phoneNumber;
  }

  public void setPhoneNumber(String phoneNumber) {
    this.phoneNumber = phoneNumber;
  }


  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }


  public String getEmployeeIdEmployee() {
    return employeeIdEmployee;
  }

  public void setEmployeeIdEmployee(String employeeIdEmployee) {
    this.employeeIdEmployee = employeeIdEmployee;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Contacts contacts = (Contacts) o;
    return idcontacts == contacts.idcontacts &&
            Objects.equals(phoneNumber, contacts.phoneNumber) &&
            Objects.equals(email, contacts.email) &&
            Objects.equals(employeeIdEmployee, contacts.employeeIdEmployee);
  }

  @Override
  public int hashCode() {
    return Objects.hash(idcontacts, phoneNumber, email, employeeIdEmployee);
  }
}
