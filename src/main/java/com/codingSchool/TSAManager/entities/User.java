package com.codingSchool.TSAManager.entities;

import javax.persistence.*;

@Entity
@Table(name="user")
public class User {
    @Id
    @Column(name="idUser")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    @Column(name="username", nullable = false, unique = true)
    private String username;
    @Column(name="password")
    private String password;

    @Transient
    private Authorization authorization;

    public Authorization getAuthorization() {
        return authorization;
    }

    public void setAuthorization(Authorization authorization) {
        this.authorization = authorization;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
