package com.codingSchool.TSAManager.entities;


import java.time.LocalDate;
import java.util.Objects;

public class Day {

  private long idday;
  private LocalDate date;
  private String shift;
  private String flag;


  public long getIdday() {
    return idday;
  }

  public void setIdday(long idday) {
    this.idday = idday;
  }


  public String getDate() {
    return date.toString();
  }

  public void setDate(LocalDate date) {
    this.date = date;
  }


  public String getShift() {
    return shift;
  }

  public void setShift(String shift) {
    this.shift = shift;
  }


  public String getFlag() {
    return flag;
  }

  public void setFlag(String flag) {
    this.flag = flag;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Day day = (Day) o;
    return idday == day.idday &&
            Objects.equals(date, day.date) &&
            Objects.equals(shift, day.shift) &&
            Objects.equals(flag, day.flag);
  }

  @Override
  public int hashCode() {
    return Objects.hash(idday, date, shift, flag);
  }
}
