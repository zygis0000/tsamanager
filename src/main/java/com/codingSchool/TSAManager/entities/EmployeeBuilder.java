package com.codingSchool.TSAManager.entities;

import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeBuilder {

    private String idEmployee;
    private String firstName;
    private String lastName;
    private String gender;
    private String identityCode;
    private String position;
    private String address;
    private String town;
    private long teamIdTeam;
    private Employee employee;
    private List<Certificate> certificateList;
    private List<Contacts> contacts;

    public EmployeeBuilder setId(String idEmployee){
        this.idEmployee=idEmployee;
        return this;
    }
    public EmployeeBuilder setFirstName(String firstName){
        this.firstName=firstName;
        return this;
    }
    public EmployeeBuilder setLastName(String lastName){
        this.lastName=lastName;
        return this;
    }
    public EmployeeBuilder setGender(String gender){
        this.gender=gender;
        return this;
    }
    public EmployeeBuilder setIdentityCode(String identityCode){
        this.identityCode=identityCode;
        return this;
    }
    public EmployeeBuilder setPosition(String position){
        this.position=position;
        return this;
    }
    public EmployeeBuilder setAddress(String address){
        this.address=address;
        return this;
    }
    public EmployeeBuilder setTown(String town){
        this.town=town;
        return this;
    }
    public EmployeeBuilder setTeamIdTeam(long teamIdTeam){
        this.teamIdTeam=teamIdTeam;
        return this;
    }

     public EmployeeBuilder setCertificateList(List<Certificate> certificateList){
         this.certificateList=certificateList;
         return this;
     }

    public EmployeeBuilder setContacts(List<Contacts> contacts){
        this.contacts=contacts;
        return this;
    }

    public Employee build(){
         employee=new Employee();
         employee.setIdEmployee(this.idEmployee);
         employee.setFirstName(this.firstName);
         employee.setLastName(this.lastName);
         employee.setGender(this.gender);
         employee.setIdentityCode(this.identityCode);
         employee.setPosition(this.position);
         employee.setAddress(this.address);
         employee.setTown(this.town);
         employee.setTeamIdTeam(this.teamIdTeam);
         if (this.certificateList!=null){
             employee.setCertificateList(certificateList);
         }
         if (this.contacts!=null){
             employee.setContacts(contacts);
         }
         return employee;
    }


}
