<%--
  Created by IntelliJ IDEA.
  User: Zygis
  Date: 2019-04-03
  Time: 19:25
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<html>
<head>
    <title>login</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="resources/css/loginStyle.css">
</head>
<body class="text-center">

<form class="form-signin" action="/login" method="POST">
    <img class="" src="resources/svg/plane.svg" alt="">
    <h1 class="h3 mb-3 font-weight-normal">Please sign in</h1>
    <label for="usr" class="sr-only">Username</label>
    <input type="text" id="usr" class="form-control" placeholder="Username" required="" autofocus="" name="username">
    <label for="psw" class="sr-only">Password</label>
    <input type="password" id="psw" class="form-control" placeholder="Password" required="" name="password">
    <div class="checkbox mb-3">
        <label>
            <input type="checkbox" value="remember-me"> Remember me
        </label>
    </div>
    <button class="btn btn-lg btn-dark btn-block" type="submit">Sign in</button>
    <p class="mt-5 mb-3 text-muted">Copyright by Zygis : 2019</p>
</form>
</body>
</html>